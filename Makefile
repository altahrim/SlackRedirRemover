PROJECT_NAME = SlackRedirectRemover
XPI_EXCLUDED_FILES = README.md
xpi:
	zip -9r ../$(PROJECT_NAME).xpi ./* -x $(XPI_EXCLUDED_FILES)
	@echo "## File available in $$(readlink -f '../$(PROJECT_NAME).xpi')"

