### SlackRedirRemover
Firefox extention allowing you to bypass redirection to https://slack-redir.net when you click on a link in your favorite Slack workspace.

## Why?
If your Slack administrator is aware of privacy, maybe he activated redirection to slack-redir. With this redirection, your Slack private URL is hidden and the website you visit is unable to recover it from HTTP header `Referer`.

If you already hide your `Referer` by another way, this redirection only slow you down. That's why I made this extension.


## Installation
You can install it directly from (addons.mozilla.org)[https://addons.mozilla.org/en-US/firefox/addon/slackredirectremover/].
