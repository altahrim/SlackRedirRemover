function removeRedirect(e) {
    var target = e.target;
    while ((target.tagName != 'A' || !target.href) && target.parentNode) {
        target = target.parentNode;
    }
    if (target.tagName != 'A') {
        return;
    }

    var parsedHref = new URL(target.href);
    if (parsedHref.hostname == 'slack-redir.net' && parsedHref.searchParams.get('url')) {
        target.href = parsedHref.searchParams.get('url');
        e.stopPropagation();
    } else if (target.href.indexOf('.slack.com/') < 0) {
        e.stopPropagation();
    }
}

function init() {
    let app = document.querySelector('div.p-workspace');
    if (!app) {
        // We have to wait until Slack is ready
        window.setTimeout(init, 1000);
        return;
    }

    // Handle all clicks
    app.addEventListener('click', removeRedirect);
    app.addEventListener('auxclick', removeRedirect);
}

init();
